+++
title = "Publications"
hascode = false
rss = "Juan Ignacio Polanco - Publications"

tags = ["research", "publications"]
+++

\newcommand{\me}{**J. I. Polanco**}

# Publications

\toc

<!-- ## Preprints -->
<!---->
<!-- @@publication-list -->
<!-- @@ -->

## Preprints

@@publication-list
- \me. **2024**.
  "Fast and accurate evaluation of Biot--Savart integrals over spatial curves".
  [[PDF]](https://arxiv.org/pdf/2406.07366v1)
  [[DOI]](https://doi.org/10.48550/arXiv.2406.07366)
  [[arXiv]](https://arxiv.org/abs/2406.07366v1)
@@

## Published

@@publication-list
- \me, S. Arun and A. Naso.
  **2023**.
  "Multi-particle Lagrangian statistics in homogeneous rotating turbulence".
  *Physical Review Fluids* 8, 034602.
  \
  [[PDF]](/assets/publications/2023/tetrads/Polanco et al. - 2023 - Multiparticle Lagrangian statistics in homogeneous.pdf)
  [[DOI]](https://doi.org/10.1103/PhysRevFluids.8.034602)
  [[arXiv]](https://arxiv.org/abs/2205.06700)
  [[BibTeX]](/assets/publications/2023/tetrads/citation.bib)

- \me, N. P. Müller and G. Krstulovic.
  **2021**.
  "Vortex Clustering, Polarisation and Circulation Intermittency in Classical
  and Quantum Turbulence".
  *Nature Communications* 12, p. 7090.
  \
  [[PDF]](https://www.nature.com/articles/s41467-021-27382-6.pdf)
  [[DOI]](https://doi.org/10.1038/s41467-021-27382-6)
  [[Code]](https://github.com/jipolanco/Circulation.jl)
  [[BibTeX]](/assets/publications/2021/polarisation/citation.bib)

- O. Outrata, M. Pavelka, J. Hron, M. La Mantia, \me and G. Krstulovic.
  **2021**.
  "On the determination of vortex ring vorticity using Lagrangian particles".
  *Journal of Fluid Mechanics* 924, A44.
  \
  [[PDF]](/assets/publications/2021/vortex_rings/Outrata et al. - 2021 - On the determination of vortex ring vorticity usin.pdf)
  [[DOI]](https://doi.org/10.1017/jfm.2021.662)
  [[arXiv]](https://arxiv.org/abs/2107.07896)
  [[BibTeX]](/assets/publications/2021/vortex_rings/citation.bib)

- N. P. Müller, \me and G. Krstulovic. **2021**.
  "Intermittency of Velocity Circulation in Quantum Turbulence".
  *Physical Review X* 11.1, p. 011053.
  \
  [[PDF]](https://journals.aps.org/prx/pdf/10.1103/PhysRevX.11.011053)
  [[DOI]](https://doi.org/10.1103/PhysRevX.11.011053)
  [[BibTeX]](/assets/publications/2021/circulation/citation.bib)

- \me and G. Krstulovic. **2020**.
  "Counterflow-Induced Inverse Energy Cascade in Three-Dimensional Superfluid
  Turbulence".
  *Physical Review Letters* 125.25, p. 254504.
  \
  [[PDF]](/assets/publications/2020/counterflow/Polanco and Krstulovic - 2020 - Counterflow-Induced Inverse Energy Cascade in Thre.pdf)
  [[DOI]](https://doi.org/10.1103/PhysRevLett.125.254504)
  [[arXiv]](https://arxiv.org/abs/2005.10106)
  [[BibTeX]](/assets/publications/2020/counterflow/citation.bib)

- \me and G. Krstulovic. **2020**.
  "Inhomogeneous distribution of particles in coflow and counterflow quantum
  turbulence".
  *Physical Review Fluids* 5.3, p. 032601.
  \
  [[PDF]](/assets/publications/2020/particles/Polanco and Krstulovic - 2020 - Inhomogeneous distribution of particles in coflow .pdf)
  [[DOI]](https://doi.org/10.1103/PhysRevFluids.5.032601)
  [[arXiv]](https://arxiv.org/abs/1910.08444)
  [[BibTeX]](/assets/publications/2020/particles/citation.bib)

- R. Ouchene, \me, I. Vinkovic and S. Simoëns. **2018**.
  "Acceleration statistics of prolate spheroidal particles in turbulent channel
  flow".
  *Journal of Turbulence* 19.10, pp. 827--848.
  \
  [[PDF]](/assets/publications/2018/spheroids/Ouchene et al_2018_Acceleration statistics of prolate spheroidal particles in turbulent channel.pdf)
  [[DOI]](https://doi.org/10.1080/14685248.2018.1516043)
  [[BibTeX]](/assets/publications/2018/spheroids/citation.bib)

- \me, I. Vinkovic, N. Stelzenmuller, N. Mordant and M. Bourgoin. **2018**.
  "Relative dispersion of particle pairs in turbulent channel flow".
  *International Journal of Heat and Fluid Flow* 71, pp. 231--245.
  \
  [[PDF]](https://arxiv.org/pdf/1804.04562.pdf)
  [[DOI]](https://doi.org/10.1016/j.ijheatfluidflow.2018.04.007)
  [[arXiv]](https://arxiv.org/abs/1804.04562)
  [[BibTeX]](/assets/publications/2018/pairs/citation.bib)

- E. Mignot, W. Cai, \me, C. Escauriaza and N. Riviere. **2017**.
  "Measurement of mass exchange processes and coefficients in a simplified
  open-channel lateral cavity connected to a main stream".
  *Environmental Fluid Mechanics* 17.3, pp. 429--448.
  \
  [[PDF]](/assets/publications/2017/cavity/Mignot et al_2017_Measurement of mass exchange processes and coefficients in a simplified.pdf)
  [[DOI]](https://doi.org/10.1007/s10652-016-9495-7)
  [[BibTeX]](/assets/publications/2017/cavity/citation.bib)

- N. Stelzenmuller, \me, L. Vignal, I. Vinkovic and N. Mordant. **2017**.
  "Lagrangian acceleration statistics in a turbulent channel flow".
  *Physical Review Fluids* 2.5, p. 054602.
  \
  [[PDF]](/assets/publications/2017/acceleration/Stelzenmuller et al_2017_Lagrangian acceleration statistics in a turbulent channel flow.pdf)
  [[DOI]](https://doi.org/10.1103/PhysRevFluids.2.054602)
  [[arXiv]](https://arxiv.org/abs/1704.07186)
  [[BibTeX]](/assets/publications/2017/acceleration/citation.bib)

- D. Stevens, H. Power, \me and A. Cliffe. **2015**.
  "A high-resolution local RBF collocation method for steady-state
  poroelasticity and hydromechanical damage analysis".
  *International Journal of Numerical and Analytical Methods in Geomechanics* 39.4, pp. 436--456.
  \
  [[PDF]](/assets/publications/2015/RBF/Stevens et al. - 2015 - A high-resolution local RBF collocation method for.pdf)
  [[DOI]](https://doi.org/10.1002/nag.2317)
  [[BibTeX]](/assets/publications/2015/RBF/citation.bib)
@@

## PhD thesis

I did my PhD at the [Fluid Mechanics and Acoustics
Laboratory (LMFA)](http://lmfa.ec-lyon.fr) and University Claude Bernard Lyon
1 under the supervision of Ivana Vinkovic.

@@publication-list
- \me. **2019**.
  "Lagrangian properties of turbulent channel flow: a numerical study".
  *Université de Lyon*.
  \
  [[PDF]](https://hal.archives-ouvertes.fr/tel-02084215v1/document)
  [[HAL]](https://hal.archives-ouvertes.fr/tel-02084215v1)
@@
