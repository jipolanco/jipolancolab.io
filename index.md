@def title = "Juan Ignacio Polanco"
@def tags = ["biography", "research"]

# About

I am a [CNRS](https://www.cnrs.fr/) researcher working at the [Geophysical and
Industrial Flows Laboratory](http://legi.grenoble-inp.fr) (LEGI) in Grenoble,
France.
My interests revolve around **[turbulent
flows](https://en.wikipedia.org/wiki/Turbulence)**, that is, the
disordered motion of fluids such as water or air flowing at relatively
high speeds.
Turbulent flows can be extremely complex to comprehend, and my focus is on
trying to describe and improve our understanding of the fundamental
mechanisms of turbulence and how these affect the dynamics of objects (such
as particles) immersed in them.
My approach is mainly a theoretical and numerical one, based on direct
numerical simulations of the equations describing turbulent flows.

@@table-history
  @@table-history-left
    2022–present
  @@
  @@table-history-right
    CNRS researcher at [LEGI](http://legi.grenoble-inp.fr) / Université Grenoble Alpes
  @@
  @@table-history-left
    2021–2022
  @@
  @@table-history-right
    Post-doctoral researcher at [LMFA](http://lmfa.ec-lyon.fr) / École Centrale de Lyon
  @@
  @@table-history-left
    2019–2021
  @@
  @@table-history-right
    Post-doctoral researcher at [Observatoire de la Côte d'Azur](https://www.oca.eu) (Nice)
  @@
  @@table-history-left
    2015–2019
  @@
  @@table-history-right
    PhD at [LMFA](http://lmfa.ec-lyon.fr) / Université Lyon 1
  @@
@@

## Contact

@@contact-info
Laboratoire LEGI \
Office A116-b \
1209--1211 rue de la piscine \
Domaine Universitaire \
38400 Saint-Martin-d'Hères, France
@@email-js
~~~
<script>
  user = 'juan-ignacio.polanco';
  site = 'univ-grenoble-alpes.fr';
  document.write('<a href=\"mailto:' + user + '@' + site + '\">');
  document.write(user + '@' + site + '</a>');
</script>
~~~
@@
@@

~~~
<div id="map">
<script>
    var map = L.map('map',{
      center: [45.19631, 5.76956],
      zoom: 13
    });

    var tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    var marker = L.marker([45.19631, 5.76956]).addTo(map);
</script>
</div>
~~~
