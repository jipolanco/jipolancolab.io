+++
title = "Job offers"
hascode = false

tags = ["jobs"]
+++

# Job offers

## PhD position: Quantum turbulence simulations using a vortex filament model

A PhD position is open at LEGI to work on simulations of quantum turbulent
flows (observed for instance in superfluid helium) at the absolute zero, where
they are described as a collection of interacting vortex filaments. The main
objective is to investigate the possible similarities between quantum and
classical turbulence, in order to strengthen the link between both systems.

\paragraph{Summary}
Superfluids, such as liquid helium at very low temperatures, are characterised
by an absence of viscosity at the absolute zero. In this limit, they are
effectively composed of very thin vortex filaments about which the velocity
circulation takes discrete values. The complex interaction between these
quantum vortices can lead to a state known as quantum turbulence, characterised
by an extremely wide range of scales going from the vortex thickness (~1 Å) to
the size of existent experimental facilities (up to ~1 metre). This state has
been shown to present quantitative similarities with classical turbulent flows,
and thus provides an interesting avenue for a better understanding of classical
turbulence. With this in mind, simulations of the vortex filament model (VFM)
will be performed in this thesis to study quantum turbulent flows. In the VFM,
vortices are described as three-dimensional curves which interact according to
Biot–Savart’s law. In particular, a reformulation of the VFM will be used for
the first time to speed-up the simulation of strongly turbulent regimes in
fully periodic domains. The proposed method will be first validated by
comparing simple cases (e.g. the collision between two vortices leading to
their reconnection) against results known from a compressible atomic-scale
model. Then, a statistical analysis of the quantum turbulent state will be
performed. The main objective is to compare high-order statistics associated to
extreme events to results known from classical turbulence, as an attempt to
further advance the analogy between both systems. In particular, statistics
associated to Eulerian and Lagrangian velocities will be considered. The motion
of vortex filaments will be tracked in time to characterise the emergence and
decay of extreme events. This study will serve as a first step towards an
accurate description of superfluid helium flows, which will require coupling
the VFM to the Navier–Stokes equations.

\figenv{
  Quantum vortex tangle from Gross–Pitaevskii simulations.}{
  /assets/images/quantum_turbulence_tangle_GP.png}{}

\paragraph{Application deadline} 12 May 2023

\paragraph{Start date} 1 October 2023

\paragraph{PhD duration} 3 years

See [PDF file](/assets/offers/2023/phd_quantum_turbulence.pdf) for more information on the project and on how to apply.
