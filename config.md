<!--
Add here global page variables to use throughout your website.
-->
+++
author = "Juan Ignacio Polanco"
date_format = "dd U yyyy"
mintoclevel = 2

# Add here files or directories that should be ignored by Franklin, otherwise
# these files might be copied and, if markdown, processed by Franklin which
# you might not want. Indicate directories by ending the name with a `/`.
# Base files such as LICENSE.md and README.md are ignored by default.
ignore = ["node_modules/"]

# RSS (the website_{title, descr, url} must be defined to get RSS)
generate_rss = true
website_title = "Juan Ignacio Polanco"
website_descr = "Juan Ignacio Polanco's personal website"
website_url   = "https://jipolanco.gitlab.io"
+++

<!--
Add here global latex commands to use throughout your pages.
-->
<!-- \newcommand{\R}{\mathbb R} -->
<!-- \newcommand{\scal}[1]{\langle #1 \rangle} -->

\newcommand{\paragraph}[1]{
~~~
<h3 style="display: inline;">#1</h3>&nbsp;&nbsp;
~~~
}

\newcommand{\figenv}[3]{
~~~
<figure style="text-align:center;">
<img src="!#2" style="#3" alt="#1"/>
<figcaption>#1</figcaption>
</figure>
~~~
}
