+++
title = "Open-source projects"
hascode = true

tags = ["code"]
+++

# Open-source projects

I develop numerical tools aimed at high-performance scientific computing and
at the analysis of data resulting from large-scale numerical simulations.
All of these tools are developed in the [Julia](https://julialang.org)
language, and are openly available under open-source licences.

- [**WriteVTK.jl**](https://github.com/JuliaVTK/WriteVTK.jl)
  allows writing VTK files for visualising of spatio-temporal data resulting
  from numerical simulations.

- [**BSplineKit.jl**](https://github.com/jipolanco/BSplineKit.jl)
  contains several tools for working with splines of arbitrary order, either for
  data interpolation, function approximation, or for numerically solving
  differential equations.

- [**PencilArrays.jl**](https://github.com/jipolanco/PencilArrays.jl)
  facilitates working with structured data arrays distributed using the MPI,
  allowing high-performance computing over thousands of computing cores.

- [**PencilFFTs.jl**](https://github.com/jipolanco/PencilFFTs.jl)
  enables the computation of fast Fourier transforms over distributed arrays
  managed by PencilArrays.jl.

- [**NonuniformFFTs.jl**](https://github.com/jipolanco/NonuniformFFTs.jl)
  is a fast parallel implementation of the [non-uniform fast Fourier
  transform](https://en.wikipedia.org/wiki/Non-uniform_discrete_Fourier_transform#Nonuniform_fast_Fourier_transform)
  (NUFFT), enabling accurate estimations of discrete Fourier
  transforms on non-equispaced locations.
